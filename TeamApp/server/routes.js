exports.routes = function routes(app) {
	var products = require('./components/products.js');
	var producers = require('./components/producers.js');
	//creating routes
	//get all method
	app.get('/products', products.getAllProducts);

	//get by id method
	app.get('/products/:id', products.getProductById);

	//add new item method
	app.post('/products', products.addProduct);

	//method for deleting products
	app.delete('/products/:id', products.delProduct);

	//method for updating products
	app.put('/products/:id', products.editProduct);

	

	//routes for producers////

	//get all producers
	app.get('/producers', producers.getAllProducers);

	// //get producer by id method
	app.get('/producers/:id', producers.getProducersProducts);

	// //add new producer method
	app.post('/producers', producers.addProducers);

	// //method for deleting producer
	app.delete('/producers/:id', producers.delProducers);

	// //method for updating producer
	app.put('/producers/:id', producers.editProducers);

};