var fs = require('fs');
var Uuid = require('node-uuid');
var bodyParser = require('body-parser');

exports.getAllProducts = function getAllProducts(req, res) {
	var productList = []; //array wich will contain products after readinf

	//using file sistem - reading info stocked on server
	fs.readdir('./components/data/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with products');
		}

		//making a loop over files and getting products
		files.forEach(function(item) {
			var obj = fs.readFileSync('./components/data/' + item, {
				encoding: 'UTF-8'
			});
			//decoding files|read content|parsing to json and pushing to result 
			productList.push(JSON.parse(obj.toString()));
		});

		//control of product list
		if (productList.length === 0) {
			return res.status(404).send('Sorry, there are no products in stock.');
		}

		res.send(productList); //sending results
	});
};

exports.getProductById = function getProductById(req, res) {
	//if we havent received id send error
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}

	var results = {}; //var wich will contain results
	var productIdExists;

	//read all files from data directory
	fs.readdir('./components/data/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with products');
		}

		//making a loop over files and getting products
		files.forEach(function(item) {

			//decoding files|read content|parsing to json and pushing to result 
			var obj = fs.readFileSync('./components/data/' + item, {
				encoding: 'UTF-8'
			});

			obj = JSON.parse(obj);

			if (obj.id === req.params.id) {
				results = obj;
				productIdExists = true;
				return res.status(200).send(results);
				console.log(productIdExists);
			}

		});
		//verifying if obj with received id was found, if yes send results

		//if id was not found send error
		if (!productIdExists) {
			res.status(404).send('Not found such ID!');
		}
	});
};

exports.addProduct = function addProduct(req, res) {
	//controll of blank property
	if (!req.body.producer) {
		return res.status(409).send('Name of producer is mandatory');
	}
	if (!req.body.model) {
		return res.status(409).send('Model of product is mandatory');
	}
	if (!req.body.description) {
		return res.status(409).send('Description is mandatory');
	}
	if (!req.body.price) {
		return res.status(409).send('Price is mandatory');
	}
	var productId = 'ID_' + Uuid.v4(); // generating new id 

	//creating an object wich will be pushed in data
	var entryData = req.body;

	// creting new property - id and
	//generating ID
	entryData.id = productId;
	entryData = JSON.stringify(entryData);

	//writing a file json with entry data info
	fs.writeFile('./components/data/' + productId + '.json', entryData, function(err) {
		if (err) {
			return res.status(401).send('Error in creating file');
		}
	});

	// respons from server
	res.send('Product with id ' + productId + ' was added succesufully!');
};

exports.delProduct = function delProduct(req, res) {
	//controll of blank property
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}

	fs.unlink('./components/data/' + req.params.id + '.json', function(err) {
		if (err) {
			return res.status(401).send('Error in deleting file');
		}

		return res.send('Product was succesufully deleted');
	});
};

exports.editProduct = function editProduct(req, res) {
	//if we havent received id send error
	if (!req.params.id) {
		return res.status(409).send('ID is mandatory');
	}
	var productIdExists;
	//read all files from data directory
	fs.readdir('./components/data/', function(err, files) {
		if (err) {
			return res.status(401).send('Failed read directory with products');
		}

		//making a loop over files and getting products)
		files.forEach(function(item) {

			//decoding files|read content|parsing to json and pushing to result 
			var obj = fs.readFileSync('./components/data/' + item, {
				encoding: 'UTF-8'
			});

			obj = JSON.parse(obj);

			// checking if our id is identical to current file id
			if (obj.id === req.params.id) {
				var input = req.body;
				input.id = req.params.id;
				fs.writeFile('./components/data/' + item, JSON.stringify(input), function(err) { //if its true modifing the file
					if (err) {
						return (401, 'Failed to add modified properties');
					}
				});
				productIdExists = true;
			}
		});

		//verifying if obj with received id was found, if yes send results
		if (productIdExists) {
			return res.status(200).send('Product properties was succesufully modified!');
		}

		//if id was not found send error
		if (!productIdExists) {
			res.status(404).send('Not found such ID!');
		}
	});
};