(function () {
	'use strict';

	angular
		.module('products')
		.factory('ConfigService', [ConfigService]);

	function ConfigService () {
		var Config = {};

		Config.apiUrl = 'http://localhost:3333/products/';
		Config.apiUrlProd = 'http://localhost:3333/producers/';

		return Config;
	}


}()); 