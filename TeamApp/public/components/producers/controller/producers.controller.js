(function () {
	'use strict';

	angular
		.module('products')
		.controller('ProducersController', ['$stateParams', '$state', '$http', 'Api', 'ConfigService', ProducersController]);

	function ProducersController ($stateParams, $state, $http, Api, ConfigService) {
		var self = this;

		self.getProducts = function getProducts (id1) {
			self.getProducerProduct(id1);
			$state.go('main.producers.producerProducts', {id: id1});
	
		};

		self.getProduct = function getProduct (id) {
			$state.go('main.product.details', {id: id});
		};


		self.getProducerProduct = function getProducerProduct(id) {
			Api.get(ConfigService.apiUrlProd + id)
				.then(function success(res) {
					self.producerProducts = res.data; 
					console.log(res);
				}, function error(error) {
					console.log(error);
				});
		};

		self.getAllProducers = function getAllProducers() {
			Api.get(ConfigService.apiUrlProd)
				.then(function success(res) {
					self.producers = res.data; 
					console.log(res);
				}, function error(error) {
					console.log(error);
				});
		};

		self.getAllProducers();
	}
}());