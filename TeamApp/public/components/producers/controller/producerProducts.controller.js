(function () {
	'use strict';

	angular
		.module('products')
		.controller('ProductController', ['$stateParams', '$state', '$http', 'Api', 'ConfigService', ProductController]);

	function ProductController ($stateParams, $state, $http, Api, ConfigService) {
		var self = this;

		self.producerProducts = []; // storring all the products

		// // redirect to edit page
		// self.isEditMode = function isEditMode () {
		// 	return $state.current.name === 'main.product.edit';
		// };

		// // redirect to add page
		// self.isAddMode = function isAddMode () {
		// 	return $state.current.name === 'main.product.add';
		// };

		//get products by id of producer
		
		


		// if addMode is true:
		// add a new product then redirects to the products page
		self.addNewProduct = function addNewProduct () {
			Api.post(ConfigService.apiUrl, self.product)
			.then(function success(response) {
				$state.go('main.products');
			}, function error(error) {
				console.log(error);
			});
		};

		// if editMode is true:
		// edit an existing product then redirects to product details
		self.editThisProduct = function editThisProduct () {
			Api.put(ConfigService.apiUrl + $state.params.id, self.product)
			.then(function success(response) {
				$state.go('main.product.details', {id: $state.params.id});
			}, function error(error) {
				console.log(error);
			});
		};

	}
}());