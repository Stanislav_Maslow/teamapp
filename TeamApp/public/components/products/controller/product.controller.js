(function () {
	'use strict';

	angular
		.module('products')
		.controller('ProductController', ['$stateParams', '$state', '$http', 'Api', 'ConfigService', ProductController]);

	function ProductController ($stateParams, $state, $http, Api, ConfigService) {
		var self = this;

		self.product = {}; // storring the created object
		self.products = []; // storring all the products

		// redirect to edit page
		self.isEditMode = function isEditMode () {
			return $state.current.name === 'main.product.edit';
		};

		// redirect to add page
		self.isAddMode = function isAddMode () {
			return $state.current.name === 'main.product.add';
		};

		// get product by id
		self.getById = function getById() {
			
			Api.get(ConfigService.apiUrl + $state.params.id) 
				.then(function success(response) {
					self.product = response.data; //object matching the id
					console.log(response);
				}, function error(error) {
					console.log(error);
				});
			
		};

		self.getById(); 

		// check what the save buttotn will do
		self.saveProduct = function saveProduct () {
			if (self.isAddMode()) {
				self.addNewProduct();
			} else if (self.isEditMode()) {
				self.editThisProduct();
			}
		};

		// if addMode is true:
		// add a new product then redirects to the products page
		self.addNewProduct = function addNewProduct () {
			Api.post(ConfigService.apiUrl, self.product)
			.then(function success(response) {
				$state.go('main.products');
			}, function error(error) {
				console.log(error);
			});
		};

		// if editMode is true:
		// edit an existing product then redirects to product details
		self.editThisProduct = function editThisProduct () {
			Api.put(ConfigService.apiUrl + $state.params.id, self.product)
			.then(function success(response) {
				$state.go('main.product.details', {id: $state.params.id});
			}, function error(error) {
				console.log(error);
			});
		};


		self.editProduct = function editProduct (id) {
			$state.go('main.product.edit', {id: id});
		};

		self.deleteProduct = function deleteProduct (id, index) {
			Api.delete (ConfigService.apiUrl + $state.params.id)
				.then(function success(response) {
					self.products.splice(index, 1);
					$state.go('main.products');
				}, function error (error) {
					console.log(error);
				});
		};
	}
}());